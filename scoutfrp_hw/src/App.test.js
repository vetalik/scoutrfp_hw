import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Failed from './failed/failed';
import Header from './Header/Header';
import Rates from './rates/rates';
import Loading from './Loading/Loading';
import { shallow } from 'enzyme';
import renderer from 'react-test-renderer';

const testData = {
  date: '2018-10-15',
  rates: {
    BGN: 1.9558,
    CAD: 1.5085,
    BRL: 4.3498,
    HUF: 323.45,
    DKK: 7.4609,
    JPY: 129.53,
    ILS: 4.2011,
    TRY: 6.686,
    RON: 4.6673,
    GBP: 0.88045,
    PHP: 62.649,
    HRK: 7.4135,
    NOK: 9.4598,
    USD: 1.1581,
    MXN: 21.8192,
    AUD: 1.6231,
    IDR: 17614.7,
    KRW: 1307.61,
    HKD: 9.0767,
    ZAR: 16.7129,
    ISK: 134.6,
    CZK: 25.798,
    THB: 37.841,
    MYR: 4.8131,
    NZD: 1.7723,
    PLN: 4.2942,
    SEK: 10.392,
    RUB: 75.924,
    CNY: 8.0135,
    SGD: 1.5945,
    CHF: 1.1428,
    INR: 85.4735
  },
  base: 'EUR'
};

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it('renders Failed', () => {
  const wrapper = shallow(<Failed error={'test Error text'} close={() => 1} />);
  expect(wrapper.find('.modal').length).toEqual(1);
  expect(wrapper.find('.modal-content').length).toEqual(1);
  expect(wrapper.find('.close').length).toEqual(1);
  expect(
    wrapper.find('.errorMessage').find({ children: 'test Error text' }).length
  ).toEqual(1);
});

it('renders Rates', () => {
  const wrapper = shallow(<Rates rates={testData.rates} />);
  expect(wrapper.find('.some-data').length).toEqual(1);
  expect(wrapper.find('table').length).toEqual(1);
  expect(wrapper.find('thead').length).toEqual(1);
  expect(wrapper.find('tbody').length).toEqual(1);
  expect(wrapper.find('td').length).toEqual(32);
  expect(wrapper.find('th').length).toEqual(32);
});

it('renders Rates snapshot correctly', () => {
  const tree = renderer.create(<Rates rates={testData.rates} />).toJSON();
  expect(tree).toMatchSnapshot();
});

it('renders Header', () => {
  const clickCallback = jest.fn();
  const wrapper = shallow(<Header onFetchClick={clickCallback} />);
  expect(wrapper.find('.App-header').length).toEqual(1);
  expect(
    wrapper.find('button').filterWhere(d => d.text() === 'Get me some rates')
      .length
  ).toEqual(1);
  wrapper.find('button').forEach(e => e.simulate('click'));
  expect(clickCallback).toBeCalled();
});

it('renders Loading', () => {
  const wrapper = shallow(<Loading />);
  expect(wrapper.find('.loading').length).toEqual(1);
  expect(
    wrapper.find('.loading').filterWhere(d => d.text() === 'Loading ...').length
  ).toEqual(1);
});
