import React, { Component } from 'react';
import './rates.css';

class Rates extends Component {
  constructor(props) {
    // Required step: always call the parent class' constructor
    super(props);
    const { rates } = props;
    // Set the state directly. Use props if necessary.
    this.state = {
      rates
    };
  }

  handleClose() {
    this.state.close();
  }

  render(props) {
    return (
      <div className="some-data">
        <table>
          <thead>
            <tr>
              {Object.entries(this.state.rates).map(([name, value]) => {
                return <th key={`${name}${value}`}>{name}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            <tr>
              {Object.entries(this.state.rates).map(([name, value]) => {
                return <td key={`${name}${value}`}>{value}</td>;
              })}
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Rates;
