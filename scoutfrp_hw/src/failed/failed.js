import React, { Component } from 'react';
import './failed.css';

class Failed extends Component {
  constructor(props) {
    // Required step: always call the parent class' constructor
    super(props);
    const { error, close } = props;
    // Set the state directly. Use props if necessary.
    this.state = {
      error,
      close
    };
    this.handleClose = this.handleClose.bind(this);
  }

  handleClose() {
    this.state.close();
  }

  render() {
    return (
      <div className="modal">
        <div className="modal-content">
          <button className="close" onClick={this.handleClose}>
            &times;
          </button>
          <p>Data Fetch Failed</p>
          <div className="errorMessage">{this.state.error}</div>
        </div>
      </div>
    );
  }
}

export default Failed;
