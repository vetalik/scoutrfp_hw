import React from 'react';
import './Header.css';

const Header = props => (
  <header className="App-header">
    <button onClick={props.onFetchClick}>Get me some rates</button>
  </header>
);

export default Header;
