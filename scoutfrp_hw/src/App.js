import React, { Component } from 'react';
import './App.css';
import Failed from './failed/failed';
import Rates from './rates/rates';
import Header from './Header/Header';
import Loading from './Loading/Loading';

import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { RestLink } from 'apollo-link-rest';
import { Query } from 'react-apollo';
import { ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
// import { onError } from "apollo-link-error";

const URL = 'https://api.exchangeratesapi.io/';

// setup your `RestLink` with your endpoint
const link = new RestLink({ uri: URL });

// setup your client
const client = new ApolloClient({
  link: link,
  cache: new InMemoryCache()
});

const query = gql`
  query date {
    rates @rest(type: "object", path: "latest") {
      rates
    }
  }
`;

class App extends Component {
  state = {
    fetch: false,
    someData: {},
    failed: false,
    error: null,
    rates: {}
  };

  onFetchClick = () => {
    this.setState({
      fetch: true
    });
  };

  closeModal = () => {
    this.setState({
      failed: false,
      error: null
    });
  };

  render() {
    return (
      <div className="App">
        <Header onFetchClick={this.onFetchClick} />

        {this.state.fetch && (
          <ApolloProvider client={client}>
            <Query query={query}>
              {({ loading, error, data }) => {
                if (loading) return <Loading />;
                if (error) {
                  return (
                    <Failed
                      error={error.networkError.result.error}
                      close={this.closeModal}
                    />
                  );
                }
                return <Rates rates={data.rates.rates} />;
              }}
            </Query>
          </ApolloProvider>
        )}
      </div>
    );
  }
}

export default App;
